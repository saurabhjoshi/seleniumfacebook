package com.selenium.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.selenium.utility.*;
import com.selenium.common.ExcelUtils;
import com.selenium.common.GlobaleVariable;
import com.selenium.common.TestBase;

public class NewTest extends TestBase {
	
	@Test(priority = 0)
	public void FirstTest() throws Exception
	{
		
		String Username = ExcelUtils.getCellData(0,1);
		String Password = ExcelUtils.getCellData(1,1);
		String Profile_Pic_Path = ExcelUtils.getCellData(2, 1);
        Factory.GetLoginPage(driver).Login(Username, Password);
      Factory.GetHomePage(driver).ClickOnUserProfileLink();
      Factory.GetHomePage(driver).UploadProfilePic(Profile_Pic_Path);
      Factory.GetHomePage(driver).ClickOnFriendsLink();
      Factory.GetHomePage(driver).PrintFriendsList();
      Factory.GetHomePage(driver).TakeSCreenShotOfTopPost();
        
	}


}
