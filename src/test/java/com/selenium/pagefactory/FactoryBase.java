package com.selenium.pagefactory;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class FactoryBase {
	
	protected static WebDriver driver;
	//protected static WebDriverWait Wait ;
	
	public FactoryBase(WebDriver d)
	{
		if(driver == null){
			driver = d;
		//FactoryBase.Wait = new WebDriverWait(driver, 100);
		}			
	}
}