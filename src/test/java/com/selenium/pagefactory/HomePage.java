package com.selenium.pagefactory;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.selenium.common.FrameWork;
import com.selenium.common.Log;
import com.selenium.utility.Constant;
import com.selenium.utility.Factory;


// This class is for the Facebook Home Page.
public class HomePage extends FactoryBase {
	
	private By link_Profile = By.cssSelector("._2s25");
	private By image_ProfilePic = By.className("profilePicThumb");
	private By image_ProfileImage = By.className("_156p");
	private By btn_UploadPic = By.xpath(".//*[@class='_3jk']/input");
	private By btn_Save = By.xpath(".//*[@class='_17ru']/div/button[2]");
	private By link_Friends = By.xpath(".//*[@data-tab-key='friends']");
	
	
	public HomePage(WebDriver d) {
		super(d);
		   if(!driver.getPageSource().contains("Home"))
		   {
			   Log.error("This is not a Facebook Home Page.");
			   throw new IllegalStateException("This is not a Facebook Home Page.");
		   }
	}
	
	  public void ClickOnUserProfileLink()
		{
		  Log.info("Clicking on Profile Link.");
		  FrameWork.Click(driver, link_Profile);
		
		}
	  
	  public void ClickOnUserProfilePic()
		{
		  Log.info("Clicking on Profile Pic.");
		  FrameWork.Click(driver, image_ProfilePic);
		
		}
	  
	  public void UploadProfilePic (String ProfilePicPath)
	  {
		  FrameWork.HoverAndClickOnElement(driver, image_ProfilePic, image_ProfileImage);
		  Log.info("Uploading Profile Pic.");
		  FrameWork.UploadFile(driver, btn_UploadPic, ProfilePicPath);
		  Log.info("Clicking on save button.");
		  FrameWork.Click(driver, btn_Save);
		  FrameWork.WaitForPageToLoad(driver, 20);
	  }
	  
	  public void ClickOnFriendsLink() throws InterruptedException
	  {
		  Log.info("Clicking on Friends link.");
		  FrameWork.Click(driver, link_Friends);
		  Thread.sleep(10000);

	  }
	  
	  public void PrintFriendsList()
	  {
		  WebDriverWait wait = new WebDriverWait(driver,60);
		  WebElement topelement = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fbTimelineHeadline")));
		  topelement.findElement(By.partialLinkText("Friends")).click();
		  Log.info("Scrolling the page it last firend is visible.");
		  do
		  {
			  
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("pagelet_timeline_medley_friends"))).sendKeys(Keys.PAGE_DOWN);
		  }while (!driver.getPageSource().contains("More About"));
		  WebElement parent = driver.findElement(By.id("pagelet_timeline_medley_friends"));
		  List<WebElement> friendsProfileBooks = parent.findElements(By.className("uiProfileBlockContent"));
		  int index = 1;
		  Log.info("Printing the friends list.");
		  for (WebElement friends : friendsProfileBooks)
		  {
			  
			  String name = friends.findElements(By.tagName("a")).get(0).getText();
			  System.out.println("Friend(" + index +"): " + name);
			  Log.info("Friend(" + index +"): " + name);
			  index++;
		  }
	 
		  
	  }
	  
	  public void TakeSCreenShotOfTopPost() throws IOException
	  {
		    WebDriverWait wait = new WebDriverWait(driver,60);
		    driver.findElement(By.xpath("//span[text()='Facebook']")).click();
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[contains(@class,'userContentWrapper')]")));
			
			WebElement contentWrapper = driver.findElement(By.xpath("//div[contains(@id,'topnews_main_stream_')]"))
					.findElement(By.xpath(".//div[contains(@class,'userContentWrapper')]"));
			
			String postText = contentWrapper.findElement(By.xpath(".//div[contains(@class,'userContent')]")).getText();
			if(!postText.isEmpty())
			{
				System.out.println("First post text: " + postText);
				Log.info("First post text: " + postText);
			}
			else{
				System.out.println("First post doesn't have content." );
				Log.info("First post doesn't have content." );
			}
			
			WebElement multimedia = contentWrapper.findElement(By.className("mtm")).findElement(By.tagName("a"));
			if (multimedia!= null)
			{	
			multimedia.click();
			
			if(multimedia.getTagName().toLowerCase().startsWith("vedio"))
			{
				System.out.println("It's vedio can not take screen shot..");
				Log.info("It's vedio can not take screen shot..");
			}
			else{
				Log.info("Taking screen shot..");
				System.out.println("Taking screen shot..");
				WebElement enlargedImg = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".stage")));
				File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				try{
					BufferedImage img = ImageIO.read(screenshot);
					org.openqa.selenium.Point point = enlargedImg.getLocation();
					int eleWidth = enlargedImg.getSize().getWidth();
					int eleHeight = enlargedImg.getSize().getHeight();
					BufferedImage eleImg= img.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
					ImageIO.write(eleImg, "png", screenshot);

					File screenshotLocation = new File("src//test//resources//" + screenshot.getName());
					System.out.println(screenshotLocation.getPath());
					Log.info(screenshotLocation.getPath());
					FileUtils.copyFile(screenshot, screenshotLocation);
				}catch(IOException e){
					System.out.println(e.getMessage());
					Log.info(e.getMessage());
				}
				
				
				enlargedImg.sendKeys(Keys.ESCAPE);
			}
			}
			else 
		    {
				System.out.println("First post doesn't have content Pic." );
				Log.info("First post doesn't have content Pic." );
			}

	
	  }
}
