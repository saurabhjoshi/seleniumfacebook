package com.selenium.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.selenium.utility.Constant;
import com.selenium.common.FrameWork;
import com.selenium.common.Log;

// This class is for Facebook Login Page.
public class LoginPage extends FactoryBase{
	
	private By txt_Email = By.id("email");
	private By txt_Pass = By.id("pass");
	private By btn_Login = By.id("u_0_l");
	
	public LoginPage(WebDriver d) {
		super(d);
		   if(!(Constant.LoginPage.PageTitle).equals(driver.getTitle()))
		   {
			   Log.error("This is not a Facebook Login Page.");
			   throw new IllegalStateException("This is not a Facebook Login Page.");
		   }
	}
	
	public void Login(String Email , String Password)
	{
		Log.info("Entering Email -> " + Email);
		FrameWork.Type(driver, txt_Email, Email);
		Log.info("Entering Password ->" + Password);
		FrameWork.Type(driver, txt_Pass, Password);
		Log.info("Clicking on Login Button");
		FrameWork.Click(driver, btn_Login);
	}

}
