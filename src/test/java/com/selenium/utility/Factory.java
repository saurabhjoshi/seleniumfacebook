package com.selenium.utility;

import org.openqa.selenium.WebDriver;

import com.selenium.pagefactory.*;

// This class Holding the constructors of all pagefactory classes.
public class Factory {

	public static LoginPage GetLoginPage(WebDriver d)
	{
		return new LoginPage(d);
	}
	
	public static HomePage GetHomePage(WebDriver d)
	{
		return new HomePage(d);
	}
	
	public static AppLib GetAppLib(WebDriver d)
	{
		return new AppLib(d);
	}
}
