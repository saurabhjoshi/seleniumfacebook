package com.selenium.utility;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.selenium.pagefactory.FactoryBase;

public class AppLib extends FactoryBase {
	
	
	public AppLib(WebDriver d)  
	{
		super(d);

	}
	
	public  void ScrollDownTillElementVisible(WebElement element)
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].scrollIntoView(true);",element);
		 
	}
	
	public  void ScrollDown()
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(0, 250);");
		 
	}
	
	public  void screenshot(WebDriver driver) 
	{

	    System.out.println("Taking the screenshot.");   
	    int count = 0;
	    File  scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
	        FileUtils.copyFile(scrFile, new File("src//test//resources//webPage_screenshot_"+count+".png"));
	    } catch (IOException e) {

	        e.printStackTrace();
	    }
	    count++;

	}
}
