package com.selenium.common;

import java.security.InvalidParameterException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;




public class FrameWork {
	
	static WebElement Element ;

	public static WebDriver InitializeBrowser(String browserType, String appURL) {
		try{
		WebDriver Initialdriver ;
		switch (browserType) {
		case "chrome":
			Initialdriver = initChromeDriver(appURL);
			break;
		case "firefox":
			Initialdriver = initFirefoxDriver(appURL);
			break;
		case "InternetExplorer":
			Initialdriver = initInternetExplorerDriver(appURL);
			break;
		default:
			throw new InvalidParameterException("Invalid Browser type..!!");
		}
		
		Initialdriver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		return Initialdriver;
	}catch (Exception e) {e.getStackTrace(); return null; }
	}

	private static WebDriver initChromeDriver(String appURL) {
		System.out.println("Launching google chrome ...");
	
		System.setProperty("webdriver.chrome.driver", GlobaleVariable.Chromedriverpath
				+ "chromedriver.exe");
		WebDriver chromedriver = new ChromeDriver();
		chromedriver.manage().window().maximize();
		chromedriver.navigate().to(appURL);
		return chromedriver;
	}
	
	private static WebDriver initInternetExplorerDriver (String appURL) {
		System.out.println("Launching Internet Explorer ...");
		
		
		System.setProperty("webdriver.ie.driver", GlobaleVariable.IEdriverpath
				+ "IEDriverServer.exe");
		WebDriver IEdriver = new InternetExplorerDriver();
		IEdriver.manage().window().maximize();
		IEdriver.navigate().to(appURL);
		
		return IEdriver;
	}

	private static WebDriver initFirefoxDriver(String appURL) {
		
		System.out.println("Launching Firefox browser..");
		WebDriver Firefoxdriver = new FirefoxDriver();
		Firefoxdriver.manage().window().maximize();
		
		Firefoxdriver.navigate().to(appURL);
		Firefoxdriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		return Firefoxdriver;
	}

    public static WebElement GetElement (WebDriver driver,By by)
    {
	return driver.findElement(by);
	
	}
    
    public static WebElement GetElement (WebDriverWait wait,By by)
    {
	return wait.until(ExpectedConditions.elementToBeClickable(by));
	
	}

	public static void SelectByVisibleText(WebDriver driver ,By by , String Text)
	{
	try{
		WebDriverWait wait = new WebDriverWait(driver, 100);
		  WebElement qty_dropdwon = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		  if (qty_dropdwon != null)
		  {
			  if (qty_dropdwon.isDisplayed())
			  {
			Select QTY =new Select(qty_dropdwon);
			QTY.selectByVisibleText(Text);
				  }else Log.error("Error is present while selecting this  -> " + qty_dropdwon );
		  }else Log.error("This Element -> " + qty_dropdwon + "is Not Found.");
	}catch (Exception e) {e.getStackTrace(); }
	}
	
	public static void SelectByValue(WebDriver driver ,By by , String Text)
	{
		 	try{
				WebDriverWait wait = new WebDriverWait(driver, 100);
				  WebElement qty_dropdwon = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
				  if (qty_dropdwon != null)
				  {
					  if (qty_dropdwon.isDisplayed())
					  {
					Select QTY =new Select(qty_dropdwon);
					QTY.selectByValue(Text);
						  }else Log.error("Error is present while selecting this  -> " + qty_dropdwon );
				  }else Log.error("This Element -> " + qty_dropdwon + "is Not Found.");
			}catch (Exception e){
				e.getStackTrace();
			                    }
	}
	
	public static void SelectByIndex(WebDriver driver ,By by , int Text)
	{
		try{
			WebDriverWait wait = new WebDriverWait(driver, 100);
			WebElement qty_dropdwon = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		  if (qty_dropdwon != null)
		  {
			  if (qty_dropdwon.isDisplayed())
			  {
			Select QTY =new Select(qty_dropdwon);
			QTY.selectByIndex(Text);
				  }else Log.error("Error is present while selecting this  -> " + qty_dropdwon );
		  }else Log.error("This Element -> " + qty_dropdwon + "is Not Found.");
	}catch (Exception e){
		e.getStackTrace();
	                    }
	}
	
	public static void Click(WebDriver driver , By by)
    {
    	try{
    		WebDriverWait wait = new WebDriverWait(driver, 100);
    	    WebElement Var = wait.until(ExpectedConditions.elementToBeClickable(by));
    		if (Var != null)
    			if (Var.isDisplayed())
    				Var.click();
    	else {
    		Log.error("Error in Clicking on -> " + Var );
    		throw new IllegalStateException("Error in Clicking on -> " + by );
    	}
    		else Log.error("This Element -> " + Var + "is Not Found.");
    }catch (Exception e){
		e.getStackTrace();
		
	}
    }
	
	public static void ClickOnImage(WebDriver driver , By XpathOfImage)
    {
    	try{
    		WebDriverWait wait = new WebDriverWait(driver, 100);
    	    WebElement Var = (WebElement) wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(XpathOfImage));
    		if (Var != null)
    			if (Var.isDisplayed())
    		Var.click();
    	else {
    		Log.error("Error in Clicking on -> " + Var );
    		throw new IllegalStateException("Error in Clicking on -> " + XpathOfImage );
    	}
    		else Log.error("This Element -> " + Var + "is Not Found.");
    }catch (Exception e){
		e.getStackTrace();
		
	}
    }
	
	
    public static void Type(WebDriver driver , By by , String value)
    {
    	try{
    		WebDriverWait wait = new WebDriverWait(driver, 5);
    	    WebElement Var = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    		//WebElement Var = driver.findElement(by);
    		if (Var != null)
    			if (Var.isDisplayed() && Var.isEnabled())
    			{
    				Var.clear();
    				Var.sendKeys(value);
    			}
    		  		
    	else {
    		Log.error("This field is disabled -> " + Var );
    	//	throw new IllegalStateException("Error in Clicking on -> " + by );
    	}
    		else Log.error("This Element -> " + Var + "is Not Found.");
    }catch (Exception e){
		e.getStackTrace();
		
	}
    }
    
	
    public static void SelectRaidoButton(WebDriver driver , By by)	
    {
    try{
		WebDriverWait wait = new WebDriverWait(driver, 100);
	    WebElement Var = wait.until(ExpectedConditions.elementToBeClickable(by));
    		if (Var != null)
    			if (Var.isDisplayed() && Var.isEnabled())
    				if (! Var.isSelected())
    		               driver.findElement(by).click();
    	        else Log.error("This field is disabled -> " + Var );
        	
    		else Log.error("This Element -> " + Var + "is Not Found.");
    }catch (Exception e){ e.getStackTrace();}
    	
    }

    public static void SelectCheckBox(WebDriver driver , By by)
    {
     try{
 		WebDriverWait wait = new WebDriverWait(driver, 100);
	    WebElement Var = wait.until(ExpectedConditions.elementToBeClickable(by));
     		if (Var != null)
     			if (Var.isDisplayed() && Var.isEnabled())
     				if (! Var.isSelected())
     		               Var.click();
     	        else Log.error("This Check Box is disabled -> " + Var );
         	
     		else Log.error("This Element -> " + Var + "is Not Found.");
     }catch (Exception e){ e.getStackTrace();}
     	
     }

    public static void DeselectCheckBox(WebDriver driver , By by)
    {
    	 try{
     		WebDriverWait wait = new WebDriverWait(driver, 100);
    	    WebElement Var = wait.until(ExpectedConditions.elementToBeClickable(by));
      		if (Var != null)
      			if (Var.isDisplayed() && Var.isEnabled())
      				if (Var.isSelected())
      		               Var.click();
      	        else Log.error("This Check Box is disabled -> " + Var );
          	
      		else Log.error("This Element -> " + Var + "is Not Found.");
      }catch (Exception e){ e.getStackTrace();}
    }
    


    public static void WaitForPageToLoad(WebDriver driver , int time)
    {
    	driver.manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
    }

    public static void AcceptAlert(WebDriver driver)
    {
    	try{
    	driver.switchTo().alert().accept();
    	Log.info(" Alert  Accepted .");
        }catch (Exception e){ e.getStackTrace();}
    }
  
     public static void DismissAlert(WebDriver driver)
    {
    	try{
        	driver.switchTo().alert().dismiss();
        	Log.error("Alert Dismissed.");
            }catch (Exception e){ e.getStackTrace();}
    }

    public static String GetText(WebDriver driver , By by)
    {
    	try{
    		WebDriverWait wait = new WebDriverWait(driver, 100);
    	    WebElement Var = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    		if (Var != null)
    			if (Var.isDisplayed())
    	return	Var.getText();
    	else {
    		Log.error("This field is not visible -> " + Var );
    	return null;
    	}
    		else {
    			Log.error("This Element -> " + Var + "is Not Found.");
    			return null;
    		}
    }catch (Exception e){
		e.getStackTrace();
		return null;
                    }
    	
	}
    
    public static String GetValuefromInputEle(WebDriver driver , By by)
    {
    	try{
    		WebDriverWait wait = new WebDriverWait(driver, 100);
    	    WebElement Var = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    		if (Var != null)
    			if (Var.isDisplayed())
    	return	Var.getAttribute("value");
    	else {
    		Log.error("This field is not visible -> " + Var );
    	return null;
    	}
    		else {
    			Log.error("This Element -> " + Var + "is Not Found.");
    			return null;
    		}
    }catch (Exception e){
		e.getStackTrace();
		return null;
                    }
    	
	}
    
    
    public static void HoverAndClickOnElement (WebDriver driver , By HoverElement , By ClickElement)
    {
    	try{
    	
    	WebElement HoverOnElement = GetElement(driver,HoverElement);
    	WebElement ClickOnElement = GetElement(driver,ClickElement);
    	if (HoverOnElement != null && ClickOnElement != null )
    	{
    		if (HoverOnElement.isDisplayed()  )
    		{
        Actions action = new Actions(driver);
        action.moveToElement(HoverOnElement).build().perform();
        Click(driver,ClickElement );
    		}else Log.error("This field is not visible -> " + HoverOnElement );
    	}else Log.error("These Elements -> " + HoverOnElement + " and "+ ClickOnElement + "are Not Found.");
    }catch (Exception e){e.getStackTrace();}

    
    }
    
    public static void HoverOn (WebDriver driver , By HoverElement )
    {
    	try{
    	
    	WebElement HoverOnElement = GetElement(driver,HoverElement);
    	
    	if (HoverOnElement != null  )
    	{
    		if (HoverOnElement.isDisplayed()  )
    		{
        Actions action = new Actions(driver);
        action.moveToElement(HoverOnElement);
        
        
    		}else Log.error("This field is not visible -> " + HoverOnElement );
    	}else Log.error("This Element -> " + HoverOnElement + "is Not Found.");
    }catch (Exception e){e.getStackTrace();}

    
    }
    
    public static void NavigateTo(WebDriver driver, String URL)
    {
    	try{
    	driver.navigate().to(URL);
    	}catch(Exception e){e.getStackTrace();}
    }
    
    public static String GetPageSorce(WebDriver driver)
    {
    	return driver.getPageSource();
    }
    
    public static void ClickOnElement(WebDriver driver, By Element)
    {
    	
    	WebElement gift = driver.findElement(Element);
		gift.click();
    }
    
    public static void UploadFile (WebDriver driver ,By Element, String PathOfFile)
    {
    	driver.findElement(Element).sendKeys(PathOfFile);
    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

	
}
	


