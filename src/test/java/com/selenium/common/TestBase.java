package com.selenium.common;



import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;





public  class TestBase {

 protected   WebDriver driver ;
 
 
  
/* @BeforeClass
 public void beforeClass() throws Exception

{	 
	
}*/

@AfterClass
 public void afterClass() throws Exception {


 }
 
@Parameters({ "browserType", "appURL" })
 @BeforeSuite
 public void beforeTest(String browserType, String url) throws Exception
 {
	 DOMConfigurator.configure("log4j.xml");
	 
	 Log.info("Launching " + browserType + "Browser..... ");
	 driver = FrameWork.InitializeBrowser(browserType, url);
	 ExcelUtils.setExcelFile(GlobaleVariable.FilePath, "Test Data");

	
}



@AfterSuite
public void AfterSuite()
    {
	driver.close();
	driver.quit();
	}
}